# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL
from trytond.pyson import Eval, Equal, Or
from trytond.transaction import Transaction
from trytond.pool import Pool


class Work(ModelSQL, ModelView):
    _name = 'project.work'

    def __init__(self):
        super(Work, self).__init__()
        self.party = copy.copy(self.party)
        if 'required' in self.party.states:
            self.party.states['required'] = Or(
                self.party.states['required'],
                Equal(Eval('type'), 'project'),
            )
        else:
            self.party.states['required'] = Equal(Eval('type'), 'project')
        if 'type' not in self.party.depends:
            self.party.depends = copy.copy(self.party.depends)
            self.party.depends.append('type')
        self._reset_columns()

    def get_invoicing_party(self, project):
        if project.type == 'task':
            project = project.parent
        return project.party

    def _compute_billing_line_vals(self, line, defaults):
        user_obj = Pool().get('res.user')

        lang = defaults['party'].lang
        if not lang:
            user = user_obj.browse(Transaction().user)
            lang = user.language

        res = []
        billing_line = {}
        billing_line['product'] = defaults['product'].id
        billing_line['party'] = defaults['party'].id
        billing_line['billing_date'] = line.date
        billing_line['quantity'] = line.hours
        billing_line['unit'] = defaults['product'].default_uom.id
        billing_line['description'] = (line.date.strftime(str(lang.date)) +
            ' ' + defaults['product'].name)
        res.append(billing_line)
        return res

Work()
